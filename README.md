# The Decent Escape

Game I made for a computer science class a while ago. I might do some updates here and there in order to make some things work better, but generally the main premise of the project will not change much. If there are any ways I should improve things either in the software itself or in one of these documents, then let me know.

## Gameplay

Get past the guy and get in your car to win.

### Controls

Use W, A, S, and D to move around. Use C to change your skin.

Press another button to do something else.

Although it may be obvious, I'll note that you must wait until after the countdown and "STARTING" text are gone before you can do anything.

## Running the game

Alright, so I think I'll add this section to clarify how exactly running the game works. There are multiple ways you can run this game.

### Running inside Processing

The "Finalproject" folder has some .pde files in it which can be run in Processing 3.5.4. They must remain inside the folder in order to be loaded. If they are not in the "Finalproject" folder, Processing will offer to create a folder with the same name and move the files into the folder. After this, you can play the game in the IDE using the Run button. That's all there is to it.

### Running outside Processing

The .pde files can be exported into a Java application able to be played outside of Processing. That is the way I initially made the Java files present in this repository, but the Java files in this repository are reduced down to the ones compiled from my work. There are numerous .jar libraries which Processing inserts into the "lib" folder along with the .jar file generated from the .pde files, but in my testing they mostly appear to not be essential to running the project. There is one library the Java application cannot run without, and that is the Processing core (named core.jar.) See the "External Libraries" subsection within the "Files" section for more details on that.

Anyways, the project is run through a shell script titled "TheDecentEscape" which reads from the .jar files inside the "lib" folder. To run the shell script, enter the location of the shell script in a computer terminal through a cd command and then enter ./TheDecentEscape  to run the file. You could also run the shell script by inserting each shell script command into the terminal individually, but that would probably be a waste of your time.

## Files

Outside of the folders are the shell script (labeled TheDecentEscape), the LICENSE, and the README.md files.

### The "source" folder

This folder contains a file named Finalproject.java, which contains the Java source code for the project all in one file. The source folder also contains the folder Finalproject, which stores the .pde files from the original version of the game (made in a program called [Processing](https://processing.org/).) The .pde files are useful in case someone wants to try exporting those files into a Java application using Processing by themselves, but I am still going to provide the already exported version here for those who either do not have that ability or are not interested. The files were made in Processing 3.5.4, so I'm not entirely sure how it might interact with Processing 4.0.1 or any later versions.

### The "lib" folder

This folder contains Finalproject.jar, which stores the .class file code stuff for the game, and its extracted version. This is also where external dependencies should go, such as Processing's core.jar. See the "External Libraries" section for details.

### External Libraries

The software relies on Processing's core.jar library. You can obtain this through this process:

- Download Processing through https://github.com/processing/processing/releases/download/processing-0270-3.5.4/processing-3.5.4-linux64.tgz* and extract the archive file.
- Pick one of these options:
    - Install and open Processing, then export the .pde files in the "Finalproject" folder into a new application. The core.jar library will generate within this new application's "lib" folder, along with other libraries which are not technically necessary.
    - Copy core.jar from /core/library in the extracted Processing archive and paste it into the "lib" folder of the pre-made Java application downloaded from this repository.

NOTE: I am trying to develop a way to make obtaining the core library easier, probably achieved through shell scripting. Stay tuned.

*I know that's not the latest version, but it's the one I made the game in (well, actually, I made the original on Windows, but GNU/Linux is a better system so I'll link to the GNU/Linux version), so that's what I'd recommend. I don't know if the game really would have any issues with other versions of Processing, but let me know if I'm wrong about that. Note that the latest version of Processing is in a different repository: https://github.com/processing/processing4/releases/download/processing-1289-4.1.1/processing-4.1.1-linux-x64.tgz

You can also download Processing versions through http://download.processing.org/processing-3.5.4-linux64.tgz (change version by changing the archive file name), but that way of doing it, I have found, does not always seem to work.

## License

Below are the license details for the software. The first section addresses the license of the software being distributed in this repository, while the second describes the license for the core.jar dependency from Processing.

### Main software license

All of the files provided in this repository are free and open-source, released under the 2-Clause BSD license.
See the LICENSE file for more details.

#### Some talk about the shell script

I feel as though the shell script file also can fall under the same license (or otherwise not cause any legal issues), even though it was initially automatically generated during the Processing application export process. I had some concerns over how this would all apply, but from what I could discern, I don't really think there's an issue. The shell script only really contains some relatively basic Java terminal commands to begin with, and I modified the file slightly later on, so I guess I don't need to worry about that. Also, a number of other programs such as Scratch tend to generate certain things for their stuff and, from what I can tell, they don't seem to be attempting to apply their copyrights to your creations. Not to mention, there's also the automatically generated MANIFEST.MF, which is in a lot of Java programs and doesn't really cause any copyright issues for obvious reasons.

So, from that, I guess things in that regard should be fine. Again, though, it would be good to let me know if I've made a mistake.

I might remove this section later on. It's just here to share some thoughts.

### External library license

The external Processing library (core.jar) is licensed under the [GNU Lesser General Public License version 2.1](https://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt).