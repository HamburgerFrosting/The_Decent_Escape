import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Finalproject extends PApplet {

/* A simple game made by HamburgerFrosting (Takes some properties from some abandoned projects [some of which were also used in a nonabandoned one.])
Copyright (c) 2021-2023 HamburgerFrosting
This file is licensed under the 2-Clause BSD License. See the LICENSE file for details.  */
Player player;
Enemy enemy;
int end = 0;
int countdown = 5;
int[] secret = {(int) (Math.random()*255), (int) (Math.random()*255), (int) (Math.random()*255)};
public void setup() {
  
  player = new Player();
  enemy = new Enemy();
}
public void draw() {
 if (end == 0) { 
  game();
  if (countdown == -1) {
    if (keyPressed == true && (key == 'Z' || (key == 'z'))) {
      play();
    }
  }
 } else if (end == 1) {
 gameover();
 } else {
 victory();
 }
}

public void game() {
  background(0);
  textSize(width/20);
  text("The Decent Escape", width/4, height/2);
  textSize(width/30);
  text("Gameplay starts in " + countdown, width/3.15f, height*0.83333333333f);
  if (countdown <= 0) {
    background(104); // The environment.
    rectMode(CORNER);
    fill(204);
    noStroke();
    rect(width/17, 0, width*0.885f, height);
    stroke(2);
    fill(255);
    rectMode(CENTER); // The car.
    rect(width/2, 1, width/6, height/12);
    strokeWeight(1);
    rect(width/2, 1, width/12, height/12);
    player.showPlayer();
    enemy.showEnemy();
    if (countdown == 0) {
      frameRate(1);
      fill(0, 255, 0);
      textSize(width/15);
      text("STARTING", width/3, height*0.83333333333f);
      countdown--;
    }
    else {
      frameRate(25);
    }
    if (enemy.x < width/5 || (enemy.x > width*4/5)) {
        enemy.direction = -enemy.direction;
    }
    if ((enemy.x == player.x) && (enemy.y == player.y))
        end = 1;
    if ((player.x == width/2) && (player.y == 0))
        end = 2;
  } else {
      frameRate(1);
      countdown--;
    }
  }

  public void play() {
    background(secret[0], secret[1], secret[2]);
    text("You found the secret screen!", width/4, height/2);
  }
  
  public void gameover() {
    background(0);
    fill(255);
    textSize(width/15);
    text("GAME OVER", width/3.5f, height/2);
  }
  
  public void victory() {
    background(0);
    fill(255);
    textSize(width/10);
    text("YOU WIN", width/3.75f, height/2);
  }

  public void keyPressed() {
    if (countdown == -1) {
      if (key == 'W' || (key == 'w')) {
        player.forward();
      } else if ((key == 'S' || (key == 's'))) {
        player.backward();
      } else if (player.x > width/10 && (key == 'A' || (key == 'a'))) {
        player.left();
      } else if (player.x < width*9/10 && (key == 'D' || (key == 'd'))) {
        player.right();
      } else if (key == 'C' || (key == 'c')) {
        player.skinChange();
      }
    }
    loop();
  }
// The class which makes the enemy. (Takes some properties from class of abandoned Unit 2 Project.)
class Enemy {
  float x; // x position of enemy.
  float y; // y position of enemy.
  int[] skin = {(int) (Math.random()*255), (int) (Math.random()*255), (int) (Math.random()*255)};
  float direction;
  Enemy() {
    direction = -(width/10);
    y = width/5;
    x = width*4/5;
  }
  public void showEnemy() {
    rectMode(CENTER);
    fill(skin[0], skin[1], skin[2]);
    stroke(0);
    rect(x, y, width/12, height/12);
    x += direction;
  }
}
// The class which makes the player. (Takes some properties from class of abandoned Unit 2 Project.)

class Player {
  float x; // x position of player.
  float y; // y position of player.
  int[] skin = {(int) (Math.random()*255), (int) (Math.random()*255), (int) (Math.random()*255)}; // skin of player.
  Player() {
    x = width/2;
    y = height/2;
  }
  public void showPlayer() {
    rectMode(CENTER);
    fill(skin[0], skin[1], skin[2]);
    stroke(0);
    rect(x, y, width/12, height/12);
//    println(x,y); // Prints coordinates
  }
 public void forward() {
      y = y - height/10;
 }
 public void backward() {
      y = y + height/10;
 }
 public void left() {
      x = x - width/10;
 }
 public void right() {
      x = x + width/10;
 }
 public void skinChange() {
       skin[0] = (int) (Math.random()*255);
       skin[1] = (int) (Math.random()*255);
       skin[2] = (int) (Math.random()*255);
     }
 }
  public void settings() {  size(300,300); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Finalproject" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
