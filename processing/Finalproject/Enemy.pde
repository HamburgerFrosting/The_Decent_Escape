/* The class which makes the enemy. (Takes some properties from class of abandoned Unit 2 Project.)
NOTE: This file is from the original version of the game made in Processing 3.
Copyright (c) 2021-2022 HamburgerFrosting
This file is licensed under the 2-Clause BSD License. See the LICENSE file for details.  */
class Enemy {
  float x; // x position of enemy.
  float y; // y position of enemy.
  int[] skin = {(int) (Math.random()*255), (int) (Math.random()*255), (int) (Math.random()*255)};
  float direction;
  Enemy() {
    direction = -(width/10);
    y = width/5;
    x = width*4/5;
  }
  void showEnemy() {
    rectMode(CENTER);
    fill(skin[0], skin[1], skin[2]);
    stroke(0);
    rect(x, y, width/12, height/12);
    x += direction;
  }
}
