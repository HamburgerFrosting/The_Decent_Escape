/* A simple game made by HamburgerFrosting (Takes some properties from some abandoned projects [some of which were also used in a nonabandoned one.])
NOTE: This file is from the original version of the game made in Processing 3.
Copyright (c) 2021-2023 HamburgerFrosting
This file is licensed under the 2-Clause BSD License. See the LICENSE file for details.  */
Player player;
Enemy enemy;
int end = 0;
int countdown = 5;
int[] secret = {(int) (Math.random()*255), (int) (Math.random()*255), (int) (Math.random()*255)};
void setup() {
  size(300,300);
  player = new Player();
  enemy = new Enemy();
}
void draw() {
 if (end == 0) { 
  game();
  if (countdown == -1) {
    if (keyPressed == true && (key == 'Z' || (key == 'z'))) {
      play();
    }
  }
 } else if (end == 1) {
 gameover();
 } else {
 victory();
 }
}

void game() {
  background(0);
  textSize(width/20);
  text("The Decent Escape", width/4, height/2);
  textSize(width/30);
  text("Gameplay starts in " + countdown, width/3.15, height*0.83333333333);
  if (countdown <= 0) {
    background(104); // The environment.
    rectMode(CORNER);
    fill(204);
    noStroke();
    rect(width/17, 0, width*0.885, height);
    stroke(2);
    fill(255);
    rectMode(CENTER); // The car.
    rect(width/2, 1, width/6, height/12);
    strokeWeight(1);
    rect(width/2, 1, width/12, height/12);
    player.showPlayer();
    enemy.showEnemy();
    if (countdown == 0) {
      frameRate(1);
      fill(0, 255, 0);
      textSize(width/15);
      text("STARTING", width/3, height*0.83333333333);
      countdown--;
    } else {
      frameRate(25);
    }
    if (enemy.x < width/5 || (enemy.x > width*4/5)) {
        enemy.direction = -enemy.direction;
    }
    if ((enemy.x == player.x) && (enemy.y == player.y))
        end = 1;
    if ((player.x == width/2) && (player.y == 0))
        end = 2;
  } else {
      frameRate(1);
      countdown--;
    }
  }

  void play() {
    background(secret[0], secret[1], secret[2]);
    text("You found the secret screen!", width/4, height/2);
  }
  
  void gameover() {
    background(0);
    fill(255);
    textSize(width/15);
    text("GAME OVER", width/3.5, height/2);
  }
  
  void victory() {
    background(0);
    fill(255);
    textSize(width/10);
    text("YOU WIN", width/3.75, height/2);
  }

  void keyPressed() {
    if (countdown == -1) {
      if (key == 'W' || (key == 'w')) {
        player.forward();
      } else if ((key == 'S' || (key == 's'))) {
        player.backward();
      } else if (player.x > width/10 && (key == 'A' || (key == 'a'))) {
        player.left();
      } else if (player.x < width*9/10 && (key == 'D' || (key == 'd'))) {
        player.right();
      } else if (key == 'C' || (key == 'c')) {
        player.skinChange();
      }
    }
    loop();
  }
