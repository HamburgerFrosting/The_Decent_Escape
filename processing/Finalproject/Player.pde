/* The class which makes the player. (Takes some properties from class of abandoned Unit 2 Project.)
NOTE: This file is from the original version of the game made in Processing 3.
Copyright (c) 2021-2023 HamburgerFrosting
This file is licensed under the 2-Clause BSD License. See the LICENSE file for details.  */
class Player {
  float x; // x position of player.
  float y; // y position of player.
  int[] skin = {(int) (Math.random()*255), (int) (Math.random()*255), (int) (Math.random()*255)}; // skin of player.
  Player() {
    x = width/2;
    y = height/2;
  }
  void showPlayer() {
    rectMode(CENTER);
    fill(skin[0], skin[1], skin[2]);
    stroke(0);
    rect(x, y, width/12, height/12);
//    println(x,y); // Prints coordinates
  }
 void forward() {
      y = y - height/10;
 }
 void backward() {
      y = y + height/10;
 }
 void left() {
      x = x - width/10;
 }
 void right() {
      x = x + width/10;
 }
 void skinChange() {
       skin[0] = (int) (Math.random()*255);
       skin[1] = (int) (Math.random()*255);
       skin[2] = (int) (Math.random()*255);
     }
 }
